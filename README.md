#SecPW
Implementation of Credentials Persisting using crypto Standards like PBKDF2

#Dependencies
Run npm install && bower install to install the dependencies

#Run Application in Dev Mode
Run grunt serve (note that Database must be running in order to start up the API)
Note that on every reload, the DB will be reset with default Credentials (christoph,christoph) bzw. (peter,peter)

#Database
We use mongoDB for Data Storage of User
Start mongoDB with mongod --config /usr/local/etc/mongod.conf
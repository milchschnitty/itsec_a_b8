'use strict';

var User = require('../../api/user/user.model');
var crypto = require('crypto');
var q = require('q');

/**
 * Authenticates a user out of an request
 * @param req
 * @returns {*} the user if authenticated, null otherwise
 */
exports.user = function(req) {
    return q.fcall(function() {
        return findUser(req.headers.username).then(function(user) {
            if (user) {
                //user found, check credentials
                if (checkIfCredentialsMatch(req, user)) {
                    return user;
                }
                return null;
            } else {
                return null;
            }
        }, function(err) {
            return null;
        });
    });
};

/**
 * Checks if the given credentials of the request match with the persistet credentials of the DB using PBKDF2 and salting.
 * @param req
 * @param user
 * @returns {boolean}
 */
function checkIfCredentialsMatch(req, user) {
    return compareHash(doPBKDF2(req.headers.password, user.salt), user.password);
}

/**
 * Checks if Header Variables Username(1-12) and Password(7-65) are set and in the in specific range
 * @param req
 * @param res
 * @returns {boolean} True if headers are set correctly, false otherwise
 */
var checkHeaders = function(req, res) {
    if (checkIfCredentialsExist(req.headers)) {
        return ((req.headers.username.length > 0 && req.headers.username.length < 13) &&
        (req.headers.password.length > 3 && req.headers.password.length < 65));
    } else {
        return false;
    }
};

/**
 * Checks if username and password are set
 * @param data
 * @returns {boolean|*}
 */
function checkIfCredentialsExist(headers) {
    return (headers.hasOwnProperty('username') && headers.hasOwnProperty('password'));
}

/**
 * Takes a passwords, salts it and returns a sha1 hash
 * @param passwd
 * @param salt
 * @returns {*}
 */
function saltAndHash(passwd, salt) {
    var shasum = crypto.createHash('sha1');
    shasum.update(passwd + salt);
    return shasum.digest('hex');
}

/**
 * Compares two Hashes
 * @param hashOne
 * @param hashTwo
 * @returns {boolean} true if both hash values match, false otherwise
 */
function compareHash(hashOne, hashTwo) {
    return hashOne === hashTwo
}

/**
 * searches for a user in the db
 * @param username
 * @returns {Object|Promise|Array|{index: number, input: string}|{}}
 */
function findUser(username) {
    return User.findOne({
        'name': username
    }).exec();
}

/**
 * Returns a key derived from given password and salt
 * Uses 512 Iterations to stretch the key and prevent Rainbow Attacks.
 * @param passwd
 * @param salt
 */
var doPBKDF2 = function(passwd, salt) {
    var key = crypto.pbkdf2Sync(passwd, salt, 4096, 512, 'sha256');
    return key.toString('hex');
};

exports.pbkdf2 = doPBKDF2;
exports.verifyHeaders = checkHeaders;
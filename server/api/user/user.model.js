'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({
    name: String,
    info: String,
    active: Boolean,
    password: String,
    failedAttempts: Number,
    salt: String
});

module.exports = mongoose.model('User', UserSchema);
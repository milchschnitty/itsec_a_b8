'use strict';

var crypto = require('crypto');
var _ = require('lodash');
var User = require('./user.model');
var auth = require('../../components/auth/authService');

// Get list of users
exports.index = function(req, res) {
    // Verify headers
    if (!auth.verifyHeaders(req, res)) {
        return res.status(403).json({'status': 'Invalid Credentials'});
    }
    auth.user(req).then(function(user) {
        if (user === null) {
            return res.status(403).json({'status': 'Invalid Credentials'});
        } else {
            User.find(function(err, users) {
                if (err) {
                    return handleError(res, err);
                }
                return res.status(200).json(users);
            });
        }
    }, function(err) {
        return res.status(500).json({'status': 'Error occured'});
    });
};

// Get a single user
exports.show = function(req, res) {
    // Verify headers
    if (!auth.verifyHeaders(req, res)) {
        return res.status(403).json({'status': 'Invalid Credentials'});
    }
    auth.user(req).then(function(user) {
        if (user === null) {
            return res.status(403).json({'status': 'Invalid Credentials'});
        } else {

            if (user._id != req.params.id) {
                return res.status(403).json({'status': 'Invalid Credentials'});
            }

            User.findById(req.params.id, function(err, user) {
                if (err) {
                    return handleError(res, err);
                }
                if (!user) {
                    return res.status(404).send('Not Foundd');
                }
                return res.json(user);
            });
        }
    }, function(err) {
        console.log('ERROR IN PROMISE; what do we do now?');
    });
};

// Creates a new user in the DB.
exports.create = function(req, res) {
    // Verify headers
    if (!auth.verifyHeaders(req, res)) {
        return res.status(403).json({'status': 'Invalid Credentials'});
    }

    //check if user already exists
    User.findOne({'name': req.headers.username}, function(err, user) {
        if (err) {
            // db error
            return res.status(500).send({'status': 'Error'});
        }
        if (!user) {
            // user does not exist
            // user model
            var newUser = {
                name: '',
                password: '',
                salt: ''
            };

            // Generate Cryptographic Salt
            try {
                var salt = crypto.randomBytes(256).toString('hex');
            }
            catch (ex) {
                return res.status(500).send({'status': 'Error'});
            }
            // populate model
            newUser.name = req.headers.username;
            // set hashed passwort
            newUser.password = auth.pbkdf2(req.headers.password, salt);
            newUser.salt = salt;


            User.create(newUser, function(err, user) {
                if (err) {
                    return res.status(500).send({'status': 'Error'});
                }
                return res.status(201).json(user);
            });
        } else {
            // user exists
            return res.status(409).send({'status': 'Already Exists'});
        }
    });

};

exports.auth = function(req, res) {
    // Verify headers
    if (!auth.verifyHeaders(req, res)) {
        return res.status(403).json({'status': 'Invalid Credentials'});
    }
    auth.user(req).then(function (user){
        if(user){
            return res.status(200).send(user);
        }else{
            return res.status(403).send({'status':'Invalid Credentials'});
        }
    },function(err){
        return res.status(500).send({'status':'Server Error'});
    });
};

function handleError(res, err) {
    return res.status(500).send(err);
}

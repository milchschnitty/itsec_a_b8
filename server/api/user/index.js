'use strict';

var express = require('express');
var controller = require('./user.controller');
var passport = require('passport');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.post('/auth', controller.auth);

module.exports = router;
'use strict';

angular.module('secpwApp')
    .controller('MainCtrl', function($rootScope, $scope, $http, $state) {
        $scope.authenticationError = false;

        if ($rootScope.authenticated) {
            $state.go('profile');
        }

        $scope.loginUser = function() {
            if ($scope.username != '' && $scope.password != '') {
                $http({
                    method: 'POST',
                    url: '/api/users/auth',
                    headers: {
                        username: $scope.username,
                        password: $scope.password
                    }
                }).then(function(response) {
                    console.log('Drinnen');
                    if (response.status == 200) {
                        $rootScope.username = $scope.username;
                        $rootScope.password = $scope.password;
                        $rootScope.id = response.data._id;
                        $rootScope.authenticated = true;
                        $state.go('profile');
                        return;
                    } else {
                        $scope.authenticationError = true;
                        $rootScope.username = '';
                        $rootScope.password = '';
                    }
                }, function(err) {
                    $scope.authenticationError = true;
                });
            } else {
                console.log('Something went wrong');
            }

        };
    });

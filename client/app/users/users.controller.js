'use strict';

angular.module('secpwApp')
    .controller('UsersCtrl', function($rootScope, $scope, $http, $state) {
        $scope.users = [];
        if (!$rootScope.authenticated) {
            $state.go('main')
        } else {
            $http({
                method: 'GET',
                url: '/api/users',
                headers: {
                    'username': $rootScope.username,
                    'password': $rootScope.password
                }
            }).then(function(resp) {
                $scope.users = resp.data;
            }, function(err) {
            });
        }

    });

'use strict';

angular.module('secpwApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('profile', {
                url: '/profile',
                templateUrl: 'app/profile/profile.html',
                controller: 'ProfileCtrl'
            });
    });
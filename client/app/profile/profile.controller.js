'use strict';

angular.module('secpwApp')
    .controller('ProfileCtrl', function($rootScope, $scope, $state, $http) {
        $scope.uname = '';
        $scope.upassword = '';
        $scope.uid = '';
        $scope.usalt = '';
        console.log('Controller');
        if (!$rootScope.authenticated) {
            $state.go('main')
        } else {
            $http({
                method: 'GET',
                url: '/api/users/' + $rootScope.id,
                headers: {
                    username: $rootScope.username,
                    password: $rootScope.password
                }
            }).then(function(response) {
                if (response.status == 200) {
                    $scope.uname = response.data.name;
                    $scope.upassword = response.data.password;
                    $scope.uid = response.data._id;
                    $scope.usalt = response.data.salt;
                    return;
                } else {
                    $scope.uname = 'ERROR';
                    $scope.upassword = 'ERROR';
                    $scope.uid = 'ERROR';
                    $scope.usalt = 'ERROR';
                }
            }, function(err) {
                $scope.authenticationError = true;
            });
        }

        $scope.logout = function() {
            $rootScope.authenticated = false;
            $rootScope.username = '';
            $rootScope.password = '';
            $rootScope.id = '';
            $state.go('main')
        }
    });

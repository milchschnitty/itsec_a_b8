'use strict';

angular.module('secpwApp')
    .config(function($stateProvider) {
        $stateProvider
            .state('register', {
                url: '/register',
                templateUrl: 'app/register/register.html',
                controller: 'RegisterCtrl'
            });
    });
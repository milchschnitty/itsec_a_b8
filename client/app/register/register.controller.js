'use strict';

angular.module('secpwApp')
    .controller('RegisterCtrl', function($rootScope, $scope, $http,$state) {

        if($rootScope.authenticated){
            $state.go('profile');
        }

        $scope.awesomeThings = [];
        $scope.passwd = '';
        $scope.username = '';
        $scope.registerError = false;
        $scope.registerSuccess = false;
        $scope.registerNewUser = function() {
            $scope.registerError = false;
            $scope.registerSuccess = false;
            if ($scope.username != '' && $scope.passwd != '') {
                $http({
                    method: 'POST',
                    url: '/api/users',
                    headers: {
                        username: $scope.username,
                        password: $scope.passwd
                    }
                }).then(function(response) {
                    console.log(response.status);
                    if (response.status != 201) {
                        $scope.registerError = true;
                    } else {
                        $scope.registerSuccess = true;
                        $rootScope.username = $scope.username;
                        $rootScope.password = $scope.passwd;
                        $rootScope.authenticated = true;
                        $rootScope.id = response.data._id;
                    }

                }, function(err) {
                    $scope.registerError = true;
                });
            } else {
                console.log('Something went wrong');
            }
        };
    });

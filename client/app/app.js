'use strict';

angular.module('secpwApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ui.router'
])
    .config(function($stateProvider, $urlRouterProvider, $locationProvider) {
        $urlRouterProvider
            .otherwise('/');

        $locationProvider.html5Mode(true);
    })
    .run(function($rootScope) {

        $rootScope.username = '';
        $rootScope.password = '';
        $rootScope.authenticated = false;
        $rootScope.id = '';
    });

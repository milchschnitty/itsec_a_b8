'use strict';

angular.module('secpwApp')
    .controller('NavbarCtrl', function($scope, $location) {
        $scope.menu = [
            {
                'title': 'Home',
                'link': '/'
            },
            {
                'title': 'users',
                'link': '/users'
            },
            {
                'title': 'register',
                'link': '/register'
            },
            {
                'title': 'profile',
                'link': '/profile'
            }
        ];

        $scope.isCollapsed = true;

        $scope.isActive = function(route) {
            return route === $location.path();
        };
    });